FROM alpine:edge

COPY etc-apk-keys /etc/apk/keys

RUN echo '@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && echo '@qluster http://packages.qluster.xyz/alpine/' >> /etc/apk/repositories \
    && apk upgrade --update-cache --available \
    && apk add --update alpine-sdk coreutils \
    && adduser -G abuild -g "Alpine Package Builder" -s /bin/ash -D builder \
    && echo "builder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    && mkdir -p /home/builder/package \
    && chown -R builder:abuild /home/builder/package

USER builder

WORKDIR /home/builder/package

VOLUME ["/home/builder/package", "/home/builder/packages/builder"]

CMD sudo apk upgrade --update-cache --available && abuild-apk update && abuild -r
